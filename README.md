# Projet_personnel_unity_2D



NOM : DUCROCQ  
Prenom : Renaud

## Description  
Ce projet est une initiative personnelle ayant pour but la prise en main de la suite Unity.  
Il est basé sur les tutoriels de la chaine [TUTO UNITY FR](https://www.youtube.com/c/TUTOUNITYFR/featured).  
Le code officiel du tutoriel est [ici](https://github.com/TUTOUNITYFR/creer-un-jeu-en-2d-facilement-unity).  
![image-1.png](https://camo.githubusercontent.com/e911fb3550ac87f124b3d7addfd1e41677d671600f0f60a8f8e4ecf3d9426aea/68747470733a2f2f7777772e7475746f756e6974792e66722f75706c6f61642f7468756d626e61696c2d736572696532442d323032302e6a7067)


